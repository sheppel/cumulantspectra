//Definitions used throughout the analysis

#include <TMath.h>

#define rapidityMin 0.05
#define rapidityMax 2.05
#define rapidityBinWidth 1.0

#define mTm0Min 0.0
#define mTm0Max 2.0
#define mTm0BinWidth 0.05

//WARNING: If you change the min, max, or width values
//make sure you change these too!
#define nRapidityBins 3
#define nmTm0Bins 40
